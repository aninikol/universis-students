import { Component, OnInit } from '@angular/core';
import { GradesService } from '../../services/grades.service';
import { GradeScale } from '../../services/grade-scale.service';
import { LoadingService} from '../../../shared/services/loading.service';

let defaultGradeScale: GradeScale;

@Component({
  selector: 'app-grades-recent',
  templateUrl: './grades-recent.component.html',
  styleUrls: ['./grades-recent.component.scss']
})
export class GradesRecentComponent implements OnInit {

  public latestCourses: any = [];
  public isCollapsed: boolean[];

  private courseTeachers: any;
  private latestExamPeriod: string;

  /* for stats box */
  public registeredCourseCount: number;
  public passedCourseCount: number;
  public failedCourseCount: number;
  public passedGradeAverage: string;


  constructor(private gradesService: GradesService, private loadingService: LoadingService ) { }

  ngOnInit() {
// show loading
    this.loadingService.showLoading();

    this.gradesService.getRecentGrades().then((res) => {
      this.latestCourses = res.value;
      this.isCollapsed = Array(this.latestCourses.length).fill(true);

      let passedCourses = this.latestCourses.filter((x) => { return x.isPassed; });
      this.passedCourseCount = passedCourses.length;
      // set failed courses
      this.failedCourseCount = this.latestCourses.filter((x) => { return !x.isPassed; }).length;
      // set sum of courses
      this.registeredCourseCount = this.passedCourseCount + this.failedCourseCount;
      // set average of passed courses
      let passedGradeAverage = this.passedCourseCount ? passedCourses.reduce((a, b) => a + b.grade1, 0)/this.passedCourseCount : 0;
      if (!defaultGradeScale) {
        // get default grade scale
        this.gradesService.getDefaultGradeScale().then((gradeScale) => {
          defaultGradeScale = gradeScale;
          this.passedGradeAverage = gradeScale.format(passedGradeAverage);
        });
      } else {
        this.passedGradeAverage = defaultGradeScale.format(passedGradeAverage);
      }
      // hide loading
      this.loadingService.hideLoading();

    });
  }
}

